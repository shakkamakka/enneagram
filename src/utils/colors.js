export const Colors = {
  darkgreen: "#0a9396",
  darkgreenRGB: "10, 147, 150",
  green: "#16a085",
  greenRGB: "22, 160, 132",
  lime: "#8bae63",
  limeRGB: "139, 174, 99",
  yellow: "#ffbc42",
  yellowRGB: "255, 188, 66",
  orange: "#ffa449",
  orangeRGB: "255, 164, 73",
  red: "#dc6e5d",
  redRGB: "220, 110, 93",
  pink: "#bb6d99",
  pinkRGB: "187, 109, 153",
  purple: "#ac4388",
  purpleRGB: "172, 67, 136",
  magenta: "#d81159",
  magentaRGB: "216, 17, 89",
};
/*
1 - 8 - 9 : body center (acting) - pink - purple - magenta
2 - 3 - 4 : heart center (feeling) - yellow - orange - red
5 - 6 - 7 : head center (thinking) - darkgreen - green - lime
*/
