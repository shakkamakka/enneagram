export const totals = {
  1: 0,
  2: 0,
  3: 0,
  4: 0,
  5: 0,
  6: 0,
  7: 0,
  8: 0,
  9: 0,
};
export const totalsPercentage = {};
export const highScores = [];

// calculate the percentage of 2 numbers
export const toPercentage = (total, number) =>
  ((Number(number) / Number(total)) * 100).toFixed(0);

// set totals in percentages (only take positive numbers)
const setTotalsInPercentage = () => {
  const totalScorePositives = Object.values(totals).reduce(
    (total, item) => total + (item > 0 ? item : 0),
    0
  );

  for (let i = 1; i <= 9; i++) {
    totalsPercentage[i] = Number(
      toPercentage(totalScorePositives, totals[i] > 0 ? totals[i] : 0)
    );
  }
};

// calculate the totals of all the types and return them in an array
export const setTotals = (list) => {
  // set totals
  for (let i = 1; i <= 9; i++) {
    totals[i] = list.reduce(
      (init, item) => (item.type === `Q${i}` ? init + item.score : init),
      0
    );
  }

  setTotalsInPercentage();

  // get highest scores
  const values = Object.values(totalsPercentage);
  const maxScore = Math.max(...values);
  highScores.length = 0;
  Object.entries(totalsPercentage).forEach((entry) => {
    const [key, value] = entry;
    if (value === maxScore) highScores.push(key);
  });
};
