import React from "react";
import QuestionRadiobox from "./QuestionRadiobox";

const Question = ({ ind, q, onChoose, isActive }) => {
  return (
    <div>
      <div className={isActive ? "question-active" : "question-inactive"}>
        <span className="form-question">{q.question}</span>
        <QuestionRadiobox index={ind} onChoose={onChoose} isActive={isActive} />
      </div>
    </div>
  );
};

export default Question;
