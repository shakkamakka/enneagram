import React from "react";
import styled from "styled-components";
import { device } from "../utils/devices";

const BarProgress = styled.div`
  display: block;
  background-color: #dedede;
  position: absolute;
  width: 100%;
  height: 3px;
  top: 0;
  left: 0;
`;
const BarProgressInner = styled.div`
  background-color: #16a085;
  min-height: 3px;
  transition: width 1s;
  width: ${(props) => props.progress}%;
  :after {
    content: "${(props) => props.progress}% ";
    color: #16a085;
    position: absolute;
    font-weight: bold;
    right: 10px;
    top: 5px;
    @media ${device.tablet} {
      content: "";
    }
  }
  :before {
    content: "Question ${(props) => props.counter} ";
    color: #16a085;
    position: absolute;
    font-weight: bold;
    left: 10px;
    top: 5px;
    @media ${device.tablet} {
      content: "";
    }
  }
`;

const FooterProgressBar = ({ progress, counter }) => {
  return (
    <BarProgress>
      <BarProgressInner progress={progress} counter={counter} />
    </BarProgress>
  );
};

export default FooterProgressBar;
