import React from "react";

const HeaderSticky = () => {
  return (
    <header className="headersticky">
      <h1>Find your Enneagram type</h1>
    </header>
  );
};

export default HeaderSticky;
