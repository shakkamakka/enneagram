import { useRef, useEffect, useState } from "react";
import ButtonShowResults from "../components/ButtonShowResults";
import Radiobox from "../components/QuestionRadiobox";

const Question = ({ ind, q, onChoose, isActive }) => {
  return (
    <div className={isActive ? "question-active" : "question-inactive"}>
      <span className="form-question">{q.question}</span>
      <Radiobox
        type={q.type}
        index={ind}
        onChoose={onChoose}
        isActive={isActive}
      />
    </div>
  );
};

const Form = ({ questions, onChoose, qActive }) => {
  const [buttonActive, setButtonActive] = useState(false);
  const scrollRef = useRef();

  useEffect(() => {
    if (scrollRef.current === null || scrollRef.current === undefined) {
      setButtonActive(true);
      console.log("not valid");
      return;
    }
    scrollRef.current.scrollIntoView(true);
  }, [onChoose]);

  return (
    <div className="form">
      {questions.map((q, index) => (
        <div key={index}>
          {qActive === index && <div className="ref" ref={scrollRef}></div>}
          <Question
            ind={index}
            q={q}
            onChoose={onChoose}
            isActive={qActive === index}
          />
        </div>
      ))}
      <ButtonShowResults active={buttonActive} />
    </div>
  );
};

export default Form;
