import React, { useEffect } from "react";
import Graph from "../components/Graph";
import { highScores } from "../utils/scores";
import { replaceAtIndex } from "../utils/functions";

const Types = {
  1: [
    "The Reformer aka The Rational",
    "Idealistic Type: Principled, Purposeful, Self-Controlled, and Perfectionistic",
  ],
  2: [
    "The Helper aka The Caring",
    "Interpersonal Type: Demonstrative, Generous, People-Pleasing, and Possessive",
  ],
  3: [
    "The Achiever aka The Success-Oriented",
    "Pragmatic Type: Adaptive, Excelling, Driven, and Image-Conscious",
  ],
  4: [
    "The Individualist aka The Sensitive",
    "Withdrawn Type: Expressive, Dramatic, Self-Absorbed, and Temperamental",
  ],
  5: [
    "The Investigator aka The Intense",
    "Cerebral Type: Perceptive, Innovative, Secretive, and Isolated",
  ],
  6: [
    "The Loyalist aka The Committed",
    "Security-Oriented Type: Engaging, Responsible, Anxious, and Suspicious",
  ],
  7: [
    "The Enthusiast aka The Busy",
    "Fun-Loving Type: Spontaneous, Versatile, Distractible, and Scattered",
  ],
  8: [
    "The Challenger aka The Powerful",
    "Dominating Type: Self-Confident, Decisive, Willful, and Confrontational",
  ],
  9: [
    "The Peacemaker aka The Easygoing",
    "Self-Effacing Type: Receptive, Reassuring, Agreeable, and Complacent",
  ],
};

const Results = () => {
  const isMultiResult = highScores.length > 1;
  let title = "";
  let subtitle = "";
  let text = "";

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const urlToWebsite = (type) =>
    `https://www.enneagraminstitute.com/type-${type}`;

  // Set txt
  if (isMultiResult) {
    let resultString = highScores.toString();
    const lastComma = resultString.lastIndexOf(",");
    const finalString = replaceAtIndex(resultString, lastComma, " or ");
    title = `Your Enneagram type is either ${finalString}!`;
    subtitle = "Read more about each type below to get a clearer understanding";
    text = [];
  } else {
    title = `Your Enneagram type is ${highScores[0]}!`;
    subtitle = Types[highScores[0]][0];
    text = Types[highScores[0]][1];
  }

  return (
    <div className="results">
      <h3>{title}</h3>
      <h2>{subtitle}</h2>
      <h4>{text}</h4>
      <Graph />
      {!isMultiResult && (
        <a className="btn" href={urlToWebsite(highScores[0])}>
          Read more about Enneagram type {highScores[0]}
        </a>
      )}
      {isMultiResult &&
        highScores.map((type) => {
          return (
            <>
              <a className="btn" href={urlToWebsite(type)}>
                Read more about Enneagram type {type}
              </a>
              <br />
            </>
          );
        })}
      <br />

      <a className="btn btn-pink" href="/">
        Redo the test
      </a>
    </div>
  );
};

export default Results;
